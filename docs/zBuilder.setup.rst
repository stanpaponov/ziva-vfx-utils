zBuilder.setup
==============



zBuilder.setup.Attributes
-------------------------

.. automodule:: zBuilder.setup.Attributes
    :members:
    :undoc-members:
    :show-inheritance:

zBuilder.setup.Constraints
--------------------------

.. automodule:: zBuilder.setup.Constraints
    :members:
    :undoc-members:
    :show-inheritance:

zBuilder.setup.Selection
------------------------

.. automodule:: zBuilder.setup.Selection
    :members:
    :undoc-members:
    :show-inheritance:
    
    
zBuilder.setup.Ziva
-------------------

.. automodule:: zBuilder.setup.Ziva
    :members:
    :undoc-members:
    :show-inheritance:


