Installation
============

Either download a zip of the repository

https://bitbucket.org/zivadynamics/ziva-vfx-utils/downloads

or clone it


https://bitbucket.org/zivadynamics/ziva-vfx-utils.git



Put the zBuilder folder and all of it's contents in a maya scripts directory

If you need to update your python path for maya you can place this in your userSetup.py::

    #!python
    import sys
    sys.path.append('/path/to/download')


